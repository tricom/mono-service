# Dockerfile for Tricom OIOUBL Catalogue Importer service 
# http://www.tricom.dk/

FROM mono

MAINTAINER Mikkel Hempel <mikkel.hempel@gmail.com>

# Add the project tarball to Docker container
ADD OioublCatalogueImporterApp /var/mono/OioublCatalogueImporterApp/
WORKDIR /var/mono/OioublCatalogueImporterApp

# Build our project
RUN nuget restore OioublCatalogueImporterApp.sln
RUN xbuild OioublCatalogueImporterApp.sln

# Change to our artifact directory
WORKDIR /var/mono/OioublCatalogueImporterApp/Importer/bin/Debug

# Entry point should be mono binary
ENTRYPOINT mono Importer.exe

CMD [ "mono",  "./Importer.exe" ]