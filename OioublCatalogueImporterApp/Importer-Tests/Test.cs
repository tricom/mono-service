﻿using NUnit.Framework;
using System;

namespace ImporterTests
{
	[TestFixture ()]
	public class Test
	{
		[Test ()]
		public void TestCase ()
		{
			// ARRANGE
			var test = 100;

			// ACT
			test++;

			// ASSERT
			Assert.AreEqual(101, test);
		}
	}
}

